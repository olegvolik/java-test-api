package com.koitechs.olegvolik.service;

import com.koitechs.olegvolik.dao.CompanyRepository;
import com.koitechs.olegvolik.domain.Company;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/** This class is the CompanyService interface implementation.
 * It consists of interface methods, overridden by means of
 * using corresponding methods of CompanyRepository.
 * @author Oleg Volik
 */
@Service
public class CompanyServiceImpl implements CompanyService {

    @Autowired
    private CompanyRepository companyRepository;

    /** Get all companies from the DB
     *
     * @return list of companies
     */
    @Override
    public List<Company> getList() {
        return companyRepository.findAll();
    }

    /** The method for searching companies by company code
     * using the Spring Data DSL method signature
     *
     * @param symbol - Company code
     * @return single company
     */
    @Override
    public Company findBySymbol(String symbol) {
        return companyRepository.findBySymbol(symbol);
    }

    /** The method for searching companies by company name
     * using the Spring Data DSL method signature
     *
     * @param companyName - Company name
     * @return single company
     */
    @Override
    public Company findByCompanyName(String companyName) {
        return companyRepository.findByCompanyName(companyName);
    }

    /** The method for retrieving the most up-to-date
     * company object by company code
     *
     * @param symbol - Company code
     * @return single company
     */
    @Override
    public Company findLatestBySymbol(String symbol) {
        return companyRepository.findLatestBySymbol(symbol);
    }

    /** The method for retrieving Top 5 companies with the
     * greatest latest volume value
     *
     * @return list of companies
     */
    @Override
    public List<Company> findCompaniesWithMaxLatestVolume() {
        return companyRepository.findCompaniesWithMaxLatestVolume();
    }

    /** The method for retrieving Top 5 companies with the
     * greatest change percent value
     *
     * @return list of companies
     */
    @Override
    public List<Company> findCompaniesWithMaxChangePercent() {
        return companyRepository.findCompaniesWithMaxChangePercent();
    }

    /** Save company to the DB
     *
     * @param company - company to save
     * @return saved company
     */
    @Override
    public Company save(Company company) {
        return companyRepository.save(company);
    }

    /** Save multiple companies to the DB
     *
     * @param companies - companies to save
     * @return collection of saved companies
     */
    @Override
    public Iterable<Company> saveAll(Iterable<Company> companies) {
        return companyRepository.saveAll(companies);
    }

    /** Delete company from the DB
     *
     * @param company - company to delete
     */
    @Override
    public void delete(Company company) {
        companyRepository.delete(company);
    }
}
