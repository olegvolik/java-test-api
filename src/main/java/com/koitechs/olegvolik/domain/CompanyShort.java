package com.koitechs.olegvolik.domain;

import lombok.Data;

/** The class is a concise representation of a company
 * given by the Stocks API. It contains the basic
 * company information.
 *
 * {@link CompanyShort}
 * @author Oleg Volik
 */
@Data
public class CompanyShort {

    private String symbol;
    private String exchange;
    private String name;
    private String date;
    private String type;
    private String iexId;
    private String region;
    private String currency;
    private String isEnabled;

    public CompanyShort() { }

    /* Getters and Setters are automatically generated, the equals() and hashCode() methods are
     * automatically overridden through the use of Lombok's @Data annotation
     */

}
