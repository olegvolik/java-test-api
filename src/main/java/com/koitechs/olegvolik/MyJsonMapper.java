package com.koitechs.olegvolik;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.koitechs.olegvolik.domain.Company;
import com.koitechs.olegvolik.service.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;

import java.net.URL;
import java.util.ArrayList;
import java.util.concurrent.RecursiveTask;

/**This class purpose is to create the initial content
 * of the database. It parses data in JSON format from
 * the API and converts it into Company objects. This
 * action is performed by the only method compute().
 * After the task is finished it repeats.
 * @author Oleg Volik
 */
public class MyJsonMapper extends RecursiveTask<ArrayList<Company>> {

    /*private final String linkFirstPart = "https://sandbox.iexapis.com/stable/stock/";
    private final String linkLastPart = "/quote?token=Tpk_ee567917a6b640bb8602834c9d30e571";*/
    private final int THRESHOLD = 200;
    private final ObjectMapper MAPPER = new ObjectMapper();
    private String[] symbols;
    private int start, end;

    // The basic service for interaction with domain objects
    @Autowired
    private CompanyService companyService;


    public MyJsonMapper (String[] symbols, int start, int end) {
        this.symbols = symbols;
        this.start = start;
        this.end = end;
    }

    @Override
    protected ArrayList<Company> compute() {
        ArrayList<Company> companies = new ArrayList<>();
        try {
            if ((end - start) < THRESHOLD) {
                for (int i = start; i < end; i++) {
                    String cLink = Constants.URLFIRSTPART.getValue().concat(symbols[i])
                            .concat(Constants.URLLASTPART.getValue());
                    URL companyApiUrl = new URL(cLink);
                    Company company = MAPPER.readValue(companyApiUrl, Company.class);
                    companies.add(company);
                }
            } else {
                int middle = (end + start) / 2;
                MyJsonMapper child1 = new MyJsonMapper(symbols, start, middle);
                MyJsonMapper child2 = new MyJsonMapper(symbols, middle, end);
                child1.fork();
                child2.fork();
                companies.addAll(child1.join());
                companies.addAll(child2.join());
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return companies;
    }
}
