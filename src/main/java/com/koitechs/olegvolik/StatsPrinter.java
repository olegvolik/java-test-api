package com.koitechs.olegvolik;

import com.koitechs.olegvolik.domain.Company;
import com.koitechs.olegvolik.service.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.TimerTask;

/**This class purpose is check the database so that
 * to determine the companies with the greatest latest
 * volume value and the companies with the greatest
 * change percent value. It also prints the Top 5 Companies
 * to the console. These actions are performed by the only
 * method run().
 * @author Oleg Volik
 */
public class StatsPrinter extends TimerTask {

    // The basic service for interaction with domain objects
    @Autowired
    private CompanyService companyService;

    public StatsPrinter () {}

    @Override
    public void run() {
        List<Company> topFiveValue = companyService.findCompaniesWithMaxLatestVolume();
        List<Company> topFiveChangePercent = companyService.findCompaniesWithMaxChangePercent();
        System.out.println("*****Top 5 Highest Value Companies*****");
        for (Company c: topFiveValue) {
            System.out.println("Company: " + c.getCompanyName() + "\t\t" + "Value: " + c.getLatestVolume());
        }
        System.out.println("*****Top 5 Companies With Greatest Change Percent*****");
        for (Company c: topFiveChangePercent) {
            System.out.println("Company: " + c.getCompanyName() + "\t\t" + "Value: " + c.getChangePercent());
        }
    }
}
