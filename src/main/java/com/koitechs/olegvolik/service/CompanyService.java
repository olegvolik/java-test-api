package com.koitechs.olegvolik.service;

import com.koitechs.olegvolik.domain.Company;

import java.util.List;

/** This interface is a service interface. It is an intermediary
 * between DAO class and controller. It declares general actions
 * can be performed to the Company objects and which reproduce the
 * repository methods.
 * @author Oleg Volik
 */
public interface CompanyService {

    /** Get all companies from the DB
     *
     * @return list of companies
     */
    List<Company> getList();

    /** The method for searching companies by company code
     * using the Spring Data DSL method signature
     *
     * @param symbol - Company code
     * @return single company
     */
    Company findBySymbol(String symbol);

    /** The method for searching companies by company name
     * using the Spring Data DSL method signature
     *
     * @param companyName - Company name
     * @return single company
     */
    Company findByCompanyName(String companyName);

    /** The method for retrieving the most up-to-date
     * company object by company code
     *
     * @param symbol - Company code
     * @return single company
     */
    Company findLatestBySymbol (String symbol);

    /** The method for retrieving Top 5 companies with the
     * greatest latest volume value
     *
     * @return list of companies
     */
    List<Company> findCompaniesWithMaxLatestVolume ();

    /** The method for retrieving Top 5 companies with the
     * greatest change percent value
     *
     * @return list of companies
     */
    List<Company> findCompaniesWithMaxChangePercent();

    /** Save company to the DB
     *
     * @param company - company to save
     * @return saved company
     */
    Company save(Company company);

    /** Save multiple companies to the DB
     *
     * @param companies - companies to save
     * @return collection of saved companies
     */
    Iterable<Company> saveAll (Iterable<Company> companies);

    /** Delete company from the DB
     *
     * @param company - company to delete
     */
    void delete(Company company);
}
