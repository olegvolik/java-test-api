package com.koitechs.olegvolik;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.koitechs.olegvolik.domain.Company;
import com.koitechs.olegvolik.service.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;

import java.net.URL;

/**This class purpose is to check current company
 * values and to compare them to the most recent
 * values stored in the DB. If the values differ,
 * the data is updated. This action is performed by
 * the only method run().
 * @author Oleg Volik
 */
public class CompanyChecker implements Runnable {

    private ObjectMapper MAPPER = new ObjectMapper();
    /*private final String URLFIRSTPART = "https://sandbox.iexapis.com/stable/stock/";
    private final String URLLASTPART = "/quote?token=Tpk_ee567917a6b640bb8602834c9d30e571";*/
    private String symbol;

    // The basic service for interaction with domain objects
    @Autowired
    private CompanyService companyService;

    public CompanyChecker (String symbol) {
        this.symbol = symbol;
    }

    @Override
    public void run() {
        try {
            URL companyUrl = new URL(Constants.URLFIRSTPART.getValue()
                    + symbol + Constants.URLLASTPART.getValue());
            Company updatedCompany = MAPPER.readValue(companyUrl, Company.class);
            Company existingCompany = companyService.findLatestBySymbol(updatedCompany.getSymbol());
            if (!updatedCompany.equals(existingCompany)) {
                companyService.save(updatedCompany);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}
