package com.koitechs.olegvolik.domain;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

/** The class is a part of the application domain layer. It is an entity class,
 * so its objects can be stored in the database. This class represents a real
 * company with its detail properties list.
 * {@link Company}
 * @author Oleg Volik
 */
@Data
@Entity
@Table(name = "companies", schema = "koitechs")
public class Company {

    //The unique Company Id number
    @Id
    @GeneratedValue
    private long id;

    @Column
    private String symbol;

    @Column
    private String companyName;

    @Column
    private String calculationPrice;

    @Column
    private float open;

    @Column
    private Date openTime;

    @Column
    private float close;

    @Column
    private Date closeTime;

    @Column
    private float high;

    @Column
    private float low;

    @Column
    private float latestPrice;

    @Column
    private String latestSource;

    @Column
    private String latestTime;

    @Column
    private Date latestUpdate;

    @Column
    private long latestVolume;

    @Column
    private float iexRealtimePrice;

    @Column
    private float iexRealtimeSize;

    @Column
    private Date iexLastUpdated;

    @Column
    private float delayedPrice;

    @Column
    private Date delayedPriceTime;

    @Column
    private float extendedPrice;

    @Column
    private float extendedChange;

    @Column
    private float extendedChangePercent;

    @Column
    private Date extendedPriceTime;

    @Column
    private float previousClose;

    @Column
    private float change;

    @Column
    private float changePercent;

    @Column
    private double iexMarketPercent;

    @Column
    private int iexVolume;

    @Column
    private long avgTotalVolume;

    @Column
    private float iexBidPrice;

    @Column
    private float iexBidSize;

    @Column
    private float iexAskPrice;

    @Column
    private float iexAskSize;

    @Column
    private long marketCap;

    @Column
    private float peRatio;

    @Column
    private float week52High;

    @Column
    private float week52Low;

    @Column
    private double ytdChange;

    @Column
    private Date createdAt;

    public Company() {}


    /*The equals() and hashCode() methods are overridden manually so that not to include "id" and
    "createdAt" fields*/
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Company company = (Company) o;
        return Float.compare(company.open, open) == 0 &&
                Float.compare(company.close, close) == 0 &&
                Float.compare(company.high, high) == 0 &&
                Float.compare(company.low, low) == 0 &&
                Float.compare(company.latestPrice, latestPrice) == 0 &&
                latestVolume == company.latestVolume &&
                Float.compare(company.iexRealtimePrice, iexRealtimePrice) == 0 &&
                Float.compare(company.iexRealtimeSize, iexRealtimeSize) == 0 &&
                Float.compare(company.delayedPrice, delayedPrice) == 0 &&
                Float.compare(company.extendedPrice, extendedPrice) == 0 &&
                Float.compare(company.extendedChange, extendedChange) == 0 &&
                Float.compare(company.extendedChangePercent, extendedChangePercent) == 0 &&
                Float.compare(company.previousClose, previousClose) == 0 &&
                Float.compare(company.change, change) == 0 &&
                Float.compare(company.changePercent, changePercent) == 0 &&
                Double.compare(company.iexMarketPercent, iexMarketPercent) == 0 &&
                iexVolume == company.iexVolume &&
                avgTotalVolume == company.avgTotalVolume &&
                Float.compare(company.iexBidPrice, iexBidPrice) == 0 &&
                Float.compare(company.iexBidSize, iexBidSize) == 0 &&
                Float.compare(company.iexAskPrice, iexAskPrice) == 0 &&
                Float.compare(company.iexAskSize, iexAskSize) == 0 &&
                marketCap == company.marketCap &&
                Float.compare(company.peRatio, peRatio) == 0 &&
                Float.compare(company.week52High, week52High) == 0 &&
                Float.compare(company.week52Low, week52Low) == 0 &&
                Double.compare(company.ytdChange, ytdChange) == 0 &&
                Objects.equals(symbol, company.symbol) &&
                Objects.equals(companyName, company.companyName) &&
                Objects.equals(calculationPrice, company.calculationPrice) &&
                Objects.equals(openTime, company.openTime) &&
                Objects.equals(closeTime, company.closeTime) &&
                Objects.equals(latestSource, company.latestSource) &&
                Objects.equals(latestTime, company.latestTime) &&
                Objects.equals(latestUpdate, company.latestUpdate) &&
                Objects.equals(iexLastUpdated, company.iexLastUpdated) &&
                Objects.equals(delayedPriceTime, company.delayedPriceTime) &&
                Objects.equals(extendedPriceTime, company.extendedPriceTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(symbol, companyName, calculationPrice, open, openTime, close, closeTime, high, low,
                latestPrice, latestSource, latestTime, latestUpdate, latestVolume, iexRealtimePrice, iexRealtimeSize,
                iexLastUpdated, delayedPrice, delayedPriceTime, extendedPrice, extendedChange, extendedChangePercent,
                extendedPriceTime, previousClose, change, changePercent, iexMarketPercent, iexVolume, avgTotalVolume,
                iexBidPrice, iexBidSize, iexAskPrice, iexAskSize, marketCap, peRatio, week52High, week52Low, ytdChange);
    }

    @PrePersist
    void createdAt() {
        this.createdAt = new Date();
    }

    /* Getters and Setters are automatically generated through the use of Lombok's @Data annotation*/

}
