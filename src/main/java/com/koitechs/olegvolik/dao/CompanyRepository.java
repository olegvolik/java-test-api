package com.koitechs.olegvolik.dao;

import com.koitechs.olegvolik.domain.Company;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/** The class is a part of the application DAO layer. The purpose of it is
 * an interaction between the application and the database. It provides methods
 * for getting the company objects from the database, saving and deleting them.
 * @author Oleg Volik
 */
@Repository
public interface CompanyRepository extends CrudRepository<Company, Long> {

    /** Get all companies from the DB
     *
     * @return list of companies
     */
    @Override
    List<Company> findAll();

    /** The method for searching companies by company code
     * using the Spring Data DSL method signature
     *
     * @param symbol - Company code
     * @return single company
     */
    Company findBySymbol (String symbol);

    /** The method for searching companies by company name
     * using the Spring Data DSL method signature
     *
     * @param companyName - Company name
     * @return single company
     */
    Company findByCompanyName (String companyName);

    /** The method for retrieving the most up-to-date
     * company object by company code
     *
     * @param symbol - Company code
     * @return single company
     */
    @Query (nativeQuery = true, value = "select * from koitechs.companies " +
            "where symbol = :symbol order by created_at desc limit 1")
    Company findLatestBySymbol (@Param("symbol") String symbol);

    /** The method for retrieving Top 5 companies with the
     * greatest latest volume value
     *
     * @return list of companies
     */
    @Query(nativeQuery = true, value = "select * from koitechs.companies " +
            "order by latest_volume desc, company_name desc limit 5")
    List<Company> findCompaniesWithMaxLatestVolume ();

    /** The method for retrieving Top 5 companies with the
     * greatest change percent value
     *
     * @return list of companies
     */
    @Query(nativeQuery = true, value = "select * from koitechs.companies " +
            "order by change_percent desc, company_name desc limit 5")
    List<Company> findCompaniesWithMaxChangePercent();

}
