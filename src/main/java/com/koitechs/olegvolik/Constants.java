package com.koitechs.olegvolik;

/** This enum contains URLs which the application
 *  uses to receive data from the API.
 *
 * @author Oleg Volik
 */
public enum Constants {

    URLFIRSTPART ("https://sandbox.iexapis.com/stable/stock/"),
    URLLASTPART ("/quote?token=Tpk_ee567917a6b640bb8602834c9d30e571"),
    STOCKSAPIURL ("https://sandbox.iexapis.com/stable/ref-data/symbols?token=Tpk_ee567917a6b640bb8602834c9d30e571");

    private String value;

    Constants (String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
