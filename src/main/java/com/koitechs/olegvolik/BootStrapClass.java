package com.koitechs.olegvolik;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.koitechs.olegvolik.domain.Company;
import com.koitechs.olegvolik.domain.CompanyShort;
import com.koitechs.olegvolik.service.CompanyService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Timer;
import java.util.concurrent.*;

/** This class is the entry point of the application.
 * Aside from it defines the application run method
 * it also provides means for additional application
 * configuration. The annotation describes the class
 * as the one can be automatically configured by
 * Spring Boot and enables automatic components
 * scanning for registering them in the Spring
 * application context. The components should be marked
 * with the appropriate annotations.
 * @author Oleg Volik
 */
@Slf4j
@SpringBootApplication
public class BootStrapClass implements CommandLineRunner {

    private final ObjectMapper MAPPER = new ObjectMapper();

    // The basic service for interaction with domain objects
    @Autowired
    private CompanyService companyService;

    @Autowired
    private BeanFactory beanFactory;

    // The service for updating the company information
    @Bean
    @Scope (value = "prototype")
    public CompanyChecker companyChecker(String symbol) {
        return new CompanyChecker(symbol);
    }

    // The service for Top 5 companies output
    @Bean
    @Scope (value = "prototype")
    public StatsPrinter statsPrinter() {
        return new StatsPrinter();
    }

    /** The main method which is invoked first to run
     * the application
     * @param args - command line arguments array
     */
    public static void main(String[] args) {
        SpringApplication.run(BootStrapClass.class, args);
    }

    @Override
    public void run(String... args) throws Exception {

        // Init state flag
        boolean isInitiated = false;

        for (;;) {
            // Creating the enabled companies list
            log.info("Building enabled companies list...");
            List<String> enabled = makeEnabledStockList();
            String[] enabledArray = enabled.toArray(new String[0]);
            log.info("The enabled companies quantity: " + enabled.size());

            // Fetching the companies data and saving them into DB
            ForkJoinPool fjp = new ForkJoinPool();
            log.info("Fetching the companies data...");
            ArrayList<Company> companies = fjp.invoke(new MyJsonMapper(enabledArray, 0, enabledArray.length));
            fjp.shutdown();
            companyService.saveAll(companies);
            log.info("Saved data from " + companies.size() + " companies");
            log.info("OK");

            if (!isInitiated) {

                // Creating tasks for companies data update
                ScheduledExecutorService exec = Executors.newScheduledThreadPool(100);
                for (String s : enabledArray) {
                    exec.scheduleAtFixedRate
                            (beanFactory.getBean(CompanyChecker.class, s), 0, 5, TimeUnit.SECONDS);
                }
                // Creating task for Top 5 output
                Timer printTimer = new Timer(false);
                printTimer.schedule(beanFactory.getBean(StatsPrinter.class), 0, 5000);
            }
            isInitiated = true;
        }
    }

    /** The method which retrieves data from Stocks API and
     * determines which of them are enabled to parse the
     * company information
     * @return list of enabled companies
     */
    private List<String> makeEnabledStockList() {
        /*final String STOCKSAPIADDRESS =
                "https://sandbox.iexapis.com/stable/ref-data/symbols?token=Tpk_ee567917a6b640bb8602834c9d30e571";*/
        URL stocksApiUrl;
        List<String> enabled = new ArrayList<>();
        try {
            stocksApiUrl = new URL(Constants.STOCKSAPIURL.getValue());
            List<CompanyShort> stockList = Arrays.asList(MAPPER.readValue(stocksApiUrl, CompanyShort[].class));
            stockList.parallelStream().filter(n -> n.getIsEnabled().equals("true")).
                    forEachOrdered(n -> enabled.add(n.getSymbol()));
        }
        catch (Exception e) {
            e.printStackTrace();
        }
            return enabled;
    }
}
